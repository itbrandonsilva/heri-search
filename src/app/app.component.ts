import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as json from './data.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public results: any[];
  public form: FormGroup;

  constructor() {
    this.form = new FormGroup({
      search: new FormControl(''),
    })
  }

  private searchData(data, search) {
    let results = [];

    data.forEach(row => {
      let { product, description } = row.record;
      if (product.toLowerCase().indexOf(search) > -1) results.push(row);
      else if (description.toLowerCase().indexOf(search) > -1) results.push(row);
    });

    return results;
  };
  
  search() {
    let search = this.form.value.search;
    if (search.length === 0) return;

    let sliced = (json as any).slice();

    search.split(" ").forEach(term => {
      let results = this.searchData(sliced, term);
      sliced = results;
    });

    this.results = sliced;
  }
}
